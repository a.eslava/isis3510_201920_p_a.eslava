package com.example.myapplication

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.CalendarView
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun btButton(view: View) {

        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
        }
        if (!mBluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivity(enableBtIntent)
        }
        val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
        startActivity(discoverableIntent)


    }

    fun getDate(date: Date): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        val fecha = formatter.format(date).toString()
        val ar = fecha.split("-").toTypedArray()
        val mes = ar[1].toInt() - 1
        return ar[0] + "-" + mes + "-" + ar[2]
    }

    fun getStudentsButton(view: View) {
        val textView = findViewById<TextView>(R.id.text)
        val requestQueue = Volley.newRequestQueue(this)

        val url =
            "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + getDate(
                Date()
            ) + "/students/201611332"
        println(url)
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
            Response.Listener { response ->

                val res = response.getJSONObject("fields").getJSONObject("name").getString("stringValue")

                if (res !== null) {
                    textView.text = res
                }
                requestQueue.stop()

            },
            Response.ErrorListener { error ->
                textView.text = "no estoy en clase :c"
                requestQueue.stop()
            }
        )

        requestQueue.add(jsonObjectRequest)

// ...
    }

    fun getMissings(view: View) {

        val enableBtIntent = Intent(this, Main2Activity::class.java)
        startActivity(enableBtIntent)

    }


}
