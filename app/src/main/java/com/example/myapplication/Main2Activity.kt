package com.example.myapplication

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CalendarView
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class Main2Activity : AppCompatActivity() {

    var prueba = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    fun getDate(date: Date): String {
        val formatter = SimpleDateFormat("dd-MM-yyyy")
        val fecha = formatter.format(date).toString()
        val ar = fecha.split("-").toTypedArray()
        val mes = ar[1].toInt() - 1
        return ar[0].toInt().toString() + "-" + mes + "-" + ar[2]
    }

    fun getMissings(view: View) {

        //getDate(Date())
        val textView = findViewById<TextView>(R.id.textoDeEstudiantes)
        val requestQueue = Volley.newRequestQueue(this)
        val calendarView = findViewById<CalendarView>(R.id.calendarView)
        requestQueue.add(JsonObjectRequest(Request.Method.GET,
            "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/" + getDate(
                Date(calendarView.date)
            ) + "/students?pageSize=1000",
            null,
            Response.Listener { response ->
                if (response.has("documents")) {
                    var estudiantesVinieron = response.getJSONArray("documents")


                    requestQueue.add(JsonObjectRequest(Request.Method.GET,
                        "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/studentsList",
                        null,
                        Response.Listener { response ->
                            var estudiantesTotal = response.getJSONArray("documents")
                            val setOfEstudiantes = mutableSetOf<String>()
                            val listaEstudiantesFaltantes = mutableListOf<JSONObject>()
                            var estudiantesParaTextView = ""
                            for (i in 0 until estudiantesVinieron.length() - 1) {
                                setOfEstudiantes.add(
                                    estudiantesVinieron.getJSONObject(i).getJSONObject("fields").getJSONObject(
                                        "code"
                                    ).getString("stringValue")

                                )
                            }
                            var csv = "name;code\n"
                            for (i in 0..(estudiantesTotal.length() - 1)) {
                                val student = estudiantesTotal.getJSONObject(i)
                                if (!setOfEstudiantes.contains(
                                        student.getJSONObject("fields").getJSONObject(
                                            "code"
                                        ).getString("stringValue")
                                    )
                                ) {
                                    listaEstudiantesFaltantes.add(student)
                                    estudiantesParaTextView += student.getJSONObject("fields").getJSONObject(
                                        "name"
                                    ).getString("stringValue") + "\n"
                                    csv += student.getJSONObject("fields").getJSONObject(
                                        "name"
                                    ).getString("stringValue") + ";"+ student.getJSONObject("fields").getJSONObject(
                                        "code"
                                    ).getString("stringValue") + ""+"\n"
                                }
                            }
                            textView.text = estudiantesParaTextView
                            println(listaEstudiantesFaltantes)
                            prueba = csv
                            val buttonView = findViewById<Button>(R.id.button5)
                            buttonView.visibility= View.VISIBLE
                        },
                        Response.ErrorListener { error ->
                            requestQueue.stop()
                        }
                    ))
                } else {
                    textView.text = "ESTA VACIO EL JSON"
                    val buttonView = findViewById<Button>(R.id.button5)
                    buttonView.visibility= View.INVISIBLE

                }
            },
            Response.ErrorListener { error ->
                requestQueue.stop()
            }

        ))

    }

    fun createCSV(view: View) {
        val textView = findViewById<TextView>(R.id.textoDeEstudiantes)
        textView.text = prueba

        var fileWriter: FileWriter? = null
        val calendarView = findViewById<CalendarView>(R.id.calendarView)
        val asd =   File(this.filesDir,getDate(
            Date(calendarView.date))+".csv")
        fileWriter = FileWriter(asd)
        println(asd.canonicalPath)
        println(asd.absolutePath)

        try {
            fileWriter.append(prueba)

        } catch (e: Exception) {
            println("Writing CSV error!")
            e.printStackTrace()
        } finally {
            try {
                fileWriter.close()

            } catch (e: IOException) {
                println("Flushing/closing error!")
                e.printStackTrace()
            }
        }
        println(asd.exists())}
}
